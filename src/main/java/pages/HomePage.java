package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends Page {

    // Elements:
    private final By loginLink = By.cssSelector(".user-info a");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    // Actions:
    public void clickLoginLink() {
        driver.findElement(loginLink).click();
    }

}
