package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Page {

    // Elements:
    private final By pageHeading = By.cssSelector(".page-header");

    protected final WebDriver driver;

    Page(WebDriver driver) {
        this.driver = driver;
        // This call sets the WebElement fields.
        PageFactory.initElements(driver, this);
    }

    // Actions:
    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getPageHeadingText() {
        return driver.findElement(pageHeading).getText();
    }
}
